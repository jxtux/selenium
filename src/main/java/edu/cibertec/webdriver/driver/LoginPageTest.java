package edu.cibertec.webdriver.driver;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.cibertec.webdriver.page.LoginPage;

public class LoginPageTest {
	private static final Logger LOG = LoggerFactory.getLogger(LoginPageTest.class);
	private static WebDriver driver;
	private String baseUrl = "https://the-internet.herokuapp.com/login";

	@BeforeClass
	public static void openDriver() {
		System.setProperty("webdriver.chrome.driver","C:\\testing\\programas\\chromedriver.exe");
		driver = new ChromeDriver();
		LOG.info("Driver creado!!!");
	}

	@AfterClass
	public static void closeDriver() {
		driver.quit();
		LOG.info("Driver terminado!!");
	}

	@Test
	public void loginValido() {
		LOG.info("---------loginValido");
		LoginPage page = new LoginPage(driver, baseUrl);

		page.open();
		page.sendUsername("tomsmith");
		page.sendPassword("SuperSecretPassword!");
		page.submitLogin();

		String message = page.getMessage();

		boolean contains = message.contains("You logged into a secure area!");
		AssertJUnit.assertTrue("Mensaje de longin Valido es fallido(no es igual al esperado)", contains);
	}

	@Test
	public void loginUserInvalido() {
		LOG.info("---------login User InValido");
		LoginPage page = new LoginPage(driver, baseUrl);

		page.open();
		page.sendUsername("xxxx");
		page.sendPassword("SuperSecretPassword!");
		page.submitLogin();

		String message = page.getMessage();

		boolean contains = message.contains("Your username is invalid!");
		AssertJUnit.assertTrue("Mensaje de User InValido es fallido(no es igual al esperado)", contains);
	}

	@Test
	public void loginPasswordInvalido() {
		LOG.info("---------password InValido");
		LoginPage page = new LoginPage(driver, baseUrl);

		page.open();
		page.sendUsername("tomsmith");
		page.sendPassword("xxxxxxxxxxx");
		page.submitLogin();

		String message = page.getMessage();

		boolean contains = message.contains("Your password is invalid!");
		AssertJUnit.assertTrue("Mensaje de Password InValido es fallido(no es igual al esperado)", contains);
	}
}
