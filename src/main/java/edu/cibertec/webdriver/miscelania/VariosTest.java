package edu.cibertec.webdriver.miscelania;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeClass;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VariosTest {
	private static final Logger LOG = LoggerFactory.getLogger(VariosTest.class);
	private static WebDriver driver;
	String baseUrl;

	@BeforeClass
	public static void openDriver() {
		System.setProperty("webdriver.chrome.driver", "C:\\testing\\programas\\chromedriver.exe");
		driver = new ChromeDriver();
		Dimension d = new Dimension(640, 480);
		driver.manage().window().setSize(d);
	}

	// @Test
	public void dimensionWindows() {
		baseUrl = "http://wikipedia.org/";
		driver.get(baseUrl);

		LOG.info("Size windows: " + driver.manage().window().getSize());
	}

	// @Test
	public void tabWindows() {
		baseUrl = "http://wikipedia.org/";
		driver.get(baseUrl);
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");

		baseUrl = "http://facebook.com/";
		driver.get(baseUrl);
	}

	// @Test
	public void refreshWindows() {
		baseUrl = "http://wikipedia.org/";
		driver.get(baseUrl);

		try {
			Thread.sleep(5000);
		} catch (Exception e) {

		}

		driver.navigate().refresh();
	}

	// @Test
	public void caracteristicasChome() {
		baseUrl = "http://wikipedia.org/";
		driver.get(baseUrl);
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();

		LOG.info("explorador: " + caps.getBrowserName() + ", version: " + caps.getVersion());
	}

	//@Test
	public void accionesJavaScript() {
		baseUrl = "http://wikipedia.org/";
		driver.get(baseUrl);
		JavascriptExecutor js = (JavascriptExecutor) driver;

		String title = js.executeScript("return document.title;").toString();
		String inner = js.executeScript("return document.documentElement.innerText;").toString();
		js.executeScript("window.scrollBy(0,350)");		
		js.executeScript("alert('hello world');");
		
		LOG.info("title: " + title);
		LOG.info("innerText: " + inner);
	}
	
//	@Test
	public void screenShot() {
		baseUrl = "http://wikipedia.org/";
		driver.get(baseUrl);
		
		File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		try {
			FileUtils.copyFile(srcFile, new File("C:\\testing\\programas\\donwload\\screen.png"));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
//	@Test
	public void linkTotal() {
		baseUrl = "http://google.com/";
		driver.get(baseUrl);
		
		driver.findElement(By.linkText("Gmail")).click();
		
		LOG.info("url: " + driver.getCurrentUrl());
	}
	
//	@Test
	public void linkParcial() {
		baseUrl = "http://wikipedia.org/";
		driver.get(baseUrl);
		
		driver.findElement(By.partialLinkText("Terms")).click();
		
		LOG.info("url: " + driver.getCurrentUrl());
	}
	
	@Test
	public void xpath() {
		baseUrl = "http://google.com/";
		driver.get(baseUrl);
		
		WebElement a = driver.findElement(By.xpath("//a[text()='Privacidad']"));
		WebElement b = driver.findElement(By.xpath("//a[@class='_Gs']"));
		LOG.info("text enlace: " + a.getText());
		LOG.info("class enlace: " + b.getText());
	}
}
