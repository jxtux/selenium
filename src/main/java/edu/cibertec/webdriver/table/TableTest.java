package edu.cibertec.webdriver.table;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TableTest {

	private static WebDriver driver;
	String baseUrl;

	@BeforeClass
	public static void openDriver() {
		System.setProperty("webdriver.chrome.driver", "C:\\testing\\programas\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	// @Test
	public void numeroFilasColumnastableDynamic() {
		String baseUrl = "http://money.rediff.com/gainers/bsc/dailygroupa?";
		driver.get(baseUrl);

		List<WebElement> col = driver.findElements(By.xpath("//*[@id='leftcontainer']/table/thead/tr/th"));
		System.out.println("Numero de columnas : " + col.size());

		List<WebElement> rows = driver.findElements(By.xpath("//*[@id='leftcontainer']/table/tbody/tr/td[1]"));
		System.out.println("Numero de filas : " + rows.size());
		driver.close();
	}

	// @Test
	public void valorUnaFilaAndColumnatableDynamic() {
		String baseUrl = "http://money.rediff.com/gainers/bsc/daily/groupa?";
		driver.get(baseUrl);

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		WebElement baseTable = driver.findElement(By.tagName("table"));

		// buscando la tercera fila de table
		WebElement tableRow = baseTable.findElement(By.xpath("//*[@id='leftcontainer']/table/tbody/tr[3]"));
		String rowtext = tableRow.getText();
		System.out.println("Tercer row of table : " + rowtext);

		// buscando la tercera fila de table y 1 columna
		WebElement tableRowText = baseTable.findElement(By.xpath("//*[@id='leftcontainer']/table/tbody/tr[3]/td[1]"));
		String valueIneed = tableRowText.getText();
		System.out.println("Valor de la Celda es : " + valueIneed);

		driver.close();
	}

	// @Test
	public void maximoValorColumnatableDynamic() throws ParseException {
		String baseUrl = "http://money.rediff.com/gainers/bsc/daily/groupa?";
		driver.get(baseUrl);
		String max;
		double m = 0, r = 0;

		// No. of Columns
		List<WebElement> col = driver.findElements(By.xpath(".//*[@id='leftcontainer']/table/thead/tr/th"));
		System.out.println("Total numero de columnas : " + col.size());

		// No.of rows
		List<WebElement> rows = driver.findElements(By.xpath(".//*[@id='leftcontainer']/table/tbody/tr/td[1]"));
		System.out.println("Total numero de filas : " + rows.size());

		for (int i = 0; i < rows.size(); i++) {
			max = driver.findElement(By.xpath("html/body/div[1]/div[5]/table/tbody/tr[" + (i + 1) + "]/td[4]"))
					.getText();
			NumberFormat f = NumberFormat.getNumberInstance();
			Number num = f.parse(max);
			max = num.toString();
			m = Double.parseDouble(max);
			if (m > r) {
				r = m;
			}
		}
		System.out.println("Maximo precio : " + r);

		driver.close();
	}

	@Test
	public void obtenerDatostableDynamic() {
		String baseUrl = "http://demo.guru99.com/selenium/table/";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(baseUrl);

		WebElement mytable = driver.findElement(By.xpath("/html/body/table/tbody"));
		List<WebElement> rows_table = mytable.findElements(By.tagName("tr"));
		int rows_count = rows_table.size();

		for (int row = 0; row < rows_count; row++) {
			List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
			int columns_count = Columns_row.size();
			System.out.println("Numbero de columnas " + row + " are " + columns_count);

			for (int column = 0; column < columns_count; column++) {
				String celtext = Columns_row.get(column).getText();
				System.out.println("Valor de la celda " + row + " y columna " + column + " es: " + celtext);
			}
			System.out.println("-------------------------------------------------- ");
		}
		
		driver.close();
	}
}
