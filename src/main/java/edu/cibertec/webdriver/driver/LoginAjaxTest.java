package edu.cibertec.webdriver.driver;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginAjaxTest {
	private static final Logger LOG = LoggerFactory.getLogger(LoginAjaxTest.class);
	private static WebDriver driver;
	private String baseUrl = "https://the-internet.herokuapp.com/dynamic_loading/2";

	@BeforeClass
	public static void openDriver() {
		System.setProperty("webdriver.chrome.driver", "C:\\testing\\programas\\chromedriver.exe");
		driver = new ChromeDriver();

		// espera 30 segundos para recien buscar en la pagina y se ejecute el codigor de
		// TEST
		// cuando el elemento sea cargado en el dom mediante AJAX
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		LOG.info("Driver creado!!!");
	}

	@AfterClass
	public static void closeDriver() {
		driver.quit();
		LOG.info("Driver terminado!!");
	}

	@Test
	public void helloWordTest() {
		LOG.info("---------Cargando a modo Ajax");

		driver.get(baseUrl);
		driver.findElement(By.cssSelector("#start > button")).click();
		String message = driver.findElement(By.cssSelector("#finish > h4")).getText();

		boolean contains = message.contains("Hello World!");
		AssertJUnit.assertTrue("Mensaje no esperado", contains);
	}

}
