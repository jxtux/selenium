package edu.cibertec.webdriver.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	private WebDriver driver;
	private String baseURL;

	public LoginPage(WebDriver driver, String baseURL) {
		super();
		this.driver = driver;
		this.baseURL = baseURL;
	}

	public void open() {
		driver.get(baseURL);
	}

	public void sendUsername(String username) {
		driver.findElement(By.id("username")).sendKeys(username);
	}

	public void sendPassword(String password) {
		driver.findElement(By.id("password")).sendKeys(password);
	}

	public void submitLogin() {
		driver.findElement(By.className("radius")).click();
	}

	public String getMessage() {
		return driver.findElement(By.id("flash")).getText();
	}
}
