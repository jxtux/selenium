package edu.cibertec.webdriver.excel;

import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertiesTest {
	private static final Logger LOG = LoggerFactory.getLogger(PropertiesTest.class);

	// @Test
	public void readProperties() throws IOException {
		Properties prop = new Properties();
		InputStream input = new FileInputStream(
				"C:\\testing\\Workspace\\selenium\\src\\main\\resources\\edu\\cibertec\\webdriver\\properties\\config.properties");
		prop.load(input);

		LOG.info(prop.getProperty("name") + " " + prop.getProperty("sueldo"));
	}

	@Test
	public void writeProperties() throws IOException {
		Properties prop = new Properties();
		OutputStream ouput = new FileOutputStream(
				"C:\\testing\\Workspace\\selenium\\src\\main\\resources\\edu\\cibertec\\webdriver\\properties\\config.properties",
				true);
		prop.setProperty ("server", "pe.server.jxtux");
		prop.store(ouput, null);
	}

}
