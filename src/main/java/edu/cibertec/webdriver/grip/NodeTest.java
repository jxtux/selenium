package edu.cibertec.webdriver.grip;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;


import java.net.MalformedURLException;
import java.net.URL;


public class NodeTest {

	private static WebDriver driver;	
	private static String nodeURL;
	private String baseUrl;

	@BeforeClass
	public static void openDriver() throws MalformedURLException {
		nodeURL = "http://192.168.1.13:5566/wd/hub";
		DesiredCapabilities capability = DesiredCapabilities.chrome();
		capability.setBrowserName("chrome");
		capability.setPlatform(Platform.WIN10);	
		
		driver = new RemoteWebDriver(new URL(nodeURL),capability);
	}

	@AfterClass
	public static void afterTest() {		
		driver.quit();
	}
	
	@Test
	public void numeroFilasColumnastableDynamic() {
		baseUrl = "http://newtours.demoaut.com";
		driver.get(baseUrl);
		
		AssertJUnit.assertEquals("Welcome: Mercury Tours", driver.getTitle());
	}
	
	

}
