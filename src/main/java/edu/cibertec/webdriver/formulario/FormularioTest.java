package edu.cibertec.webdriver.formulario;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class FormularioTest {

	private static WebDriver driver;
	String baseUrl;

	@BeforeClass
	public static void openDriver() {
		System.setProperty("webdriver.chrome.driver", "C:\\testing\\programas\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	// @Test
	public void loginValido() {
		// LOGIN forma 1
		baseUrl = "http://demo.guru99.com/selenium/webform/login.html";
		driver.get(baseUrl);

		WebElement email = driver.findElement(By.id("email"));
		WebElement password = driver.findElement(By.name("passwd"));
		WebElement login = driver.findElement(By.id("SubmitLogin"));

		email.sendKeys("abcd@gmail.com");
		password.sendKeys("abcdefghlkjl");
		login.click();

		// email.clear();
		// password.clear();
		// driver.close();
	}

	// @Test
	public void login2() {
		// LOGIN forma 2
		baseUrl = "http://demo.guru99.com/selenium/webform/login.html";
		driver.get(baseUrl);
		driver.findElement(By.id("email")).sendKeys("abcd@gmail.com");
		driver.findElement(By.name("passwd")).sendKeys("abcdefghlkjl");
		driver.findElement(By.id("SubmitLogin")).submit();

		// driver.close();
	}

	// @Test
	public void radioButtoncheckbox() {

		// RADIO BUTTON Y OPTION
		baseUrl = "http://demo.guru99.com/selenium/webform/radio.html";
		driver.get(baseUrl);
		WebElement radio2 = driver.findElement(By.id("vfb-7-2"));
		WebElement option1 = driver.findElement(By.id("vfb-6-0"));
		WebElement option3 = driver.findElement(By.id("vfb-6-2"));

		radio2.click();
		option1.click();
		option3.click();

		if (option1.isSelected()) {
			System.out.println("Checkbox is Toggled On");

		} else {
			System.out.println("Checkbox is Toggled Off");
		}
		// driver.close();
	}

	// @Test
	public void verificarCheck() {

		// Verificar si esta check
		baseUrl = "http://demo.guru99.com/selenium/facebook.html";
		driver.get(baseUrl);
		WebElement chkFBPersist = driver.findElement(By.id("persist_box"));

		for (int i = 0; i < 2; i++) {
			chkFBPersist.click();
			System.out.println("Facebook Persists Checkbox Status is -  " + chkFBPersist.isSelected());
		}
		// driver.close();
	}

	@Test
	public void selectFormulario() throws Exception {
		// SELECT Simple
		String baseURL = "http://demo.guru99.com/selenium/newtours/register.php";
		driver.get(baseURL);

		Select drpCountry = new Select(driver.findElement(By.name("country")));
		drpCountry.selectByVisibleText("ANTARCTICA");

		// SELECT Multiple
		driver.get("http://jsbin.com/osebed/2");
		Select fruits = new Select(driver.findElement(By.id("fruits")));

		fruits.selectByIndex(1);
		fruits.selectByVisibleText("Apple");
		fruits.selectByValue("grape");
		takeSnapShot(driver, "C:\\testing\\programas\\donwload\\image.png");
	}

	public static void takeSnapShot(WebDriver webdriver, String fileWithPath) throws IOException{

		TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		File DestFile = new File(fileWithPath);
		FileUtils.copyFile(SrcFile, DestFile);
	}

}
