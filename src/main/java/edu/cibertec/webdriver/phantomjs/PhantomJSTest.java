package edu.cibertec.webdriver.phantomjs;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;

public class PhantomJSTest {

	private static WebDriver driver;
	String baseUrl;

	@BeforeClass
	public static void openDriver() {
		File file = new File("C:\\testing\\programas\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
		System.setProperty("phantomjs.binary.path", file.getAbsolutePath());

		driver = new PhantomJSDriver();
	}

	@Test
	public void testingSinUi() {
		String baseUrl = "https://www.google.com";
		driver.get(baseUrl);

		WebElement element = driver.findElement(By.name("q"));
		element.sendKeys("antonio");
		element.submit();

		System.out.println("Page title is: " + driver.getTitle());
		driver.quit();
	}
}
