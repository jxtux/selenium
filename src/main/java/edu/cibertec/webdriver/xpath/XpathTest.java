package edu.cibertec.webdriver.xpath;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class XpathTest {

	private static WebDriver driver;
	String baseUrl;

	@BeforeClass
	public static void openDriver() {
		System.setProperty("webdriver.chrome.driver", "C:\\testing\\programas\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	// @Test
	public void containsFollowingSiblingTable() {
		String baseUrl = "http://demo.guru99.com/selenium/guru99home/";
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(baseUrl);

		/*
		 * Elemento de búsqueda dentro del 'Curso popular' que son hermanos del control
		 * 'SELENIUM', Aquí primero encontraremos un h2 cuyo texto es '' Algunos de
		 * nuestros cursos más populares ', luego pasamos a su elemento padre que es un'
		 * div ', dentro de este div encontraremos un enlace cuyo texto es' SELENIUM 'y
		 * al final encontraremos todos los elementos hermanos de este
		 * enlace('SELENIUM')
		 */
		List<WebElement> dateBox = driver.findElements(By.xpath(
				"//h2[contains(text(),'A few of our most popular courses')]/parent::div//div[//a[text()='SELENIUM']]/following-sibling::div[@class='rt-grid-2 rt-omega']"));

		for (WebElement webElement : dateBox) {
			System.out.println(webElement.getText());
		}

		driver.close();
	}

	@Test
	public void ancestorTable() {
		String baseUrl = "http://demo.guru99.com/selenium/guru99home/";
		driver.get(baseUrl);

		List<WebElement> dateBox = driver.findElements(By.xpath(
				"//div[.//a[text()='SELENIUM']]/ancestor::div[@class='rt-grid-2 rt-omega']/following-sibling::div"));

		for (WebElement webElement : dateBox) {
			System.out.println(webElement.getText());
		}

		driver.quit();
	}

}
