package edu.cibertec.webdriver.miscelania;

import org.testng.annotations.Test;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VariosTest2 {
	private static final Logger LOG = LoggerFactory.getLogger(VariosTest2.class);
	private static WebDriver driver;
	String baseUrl;

	@BeforeClass
	public static void openDriver() {
		System.setProperty("webdriver.chrome.driver", "C:\\testing\\programas\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--lang=en-GB");
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	// @Test
	public void esperaImplicit() {
		baseUrl = "http://google.com/";
		driver.get(baseUrl);
		WebElement element = driver.findElement(By.id("lst-ib"));

		LOG.info("element: " + element);
	}

	// @Test
	public void esperaExplicita() {
		baseUrl = "http://google.com/";
		driver.get(baseUrl);

		WebDriverWait wait = new WebDriverWait(driver, 10);

		try {
			wait.until(ExpectedConditions.titleContains("Google"));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// @Test
	public void pageLoadTimeout() {
		baseUrl = "http://google.com/";
		driver.manage().timeouts().pageLoadTimeout(4, TimeUnit.SECONDS);
		driver.get(baseUrl);

		try {
			WebElement element = driver.findElement(By.id("lst-ib"));
			LOG.info("element: " + element);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// @Test
	public void todosLinks() {
		baseUrl = "http://google.com/";
		driver.get(baseUrl);

		try {
			List<WebElement> l = driver.findElements(By.tagName("a"));
			for (WebElement element : l) {
				LOG.info("element: " + element);
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// @Test
	public void keysSeleccionarTodo() {
		baseUrl = "http://google.com/";
		driver.get(baseUrl);

		try {
			driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "a");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// @Test
	public void aumentarDimenuirTamaño() {
		baseUrl = "http://google.com/";
		driver.get(baseUrl);

		try {
			driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.ADD);
			driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.SUBTRACT);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// @Test
	public void numeroFilasTablas() {
		baseUrl = "https://en.wikipedia.org/wiki/Flag_Day_(Australia)";
		driver.get(baseUrl);

		try {
			List<WebElement> l = driver.findElements(By.cssSelector("table.infobox.vevent>tbody>tr"));
			LOG.info("numero de filas: " + l.size());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// @Test
	public void mouseHoverActions() {
		baseUrl = "https://en.wikipedia.org";
		driver.get(baseUrl);
		Actions actions = new Actions(driver);

		try {
			WebElement element = driver.findElement(By.linkText("Geography"));
			actions.moveToElement(element);

			Thread.sleep(5000);
			actions.click().build().perform();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Test(priority=1 , enabled=true)
	public void ultimaMpdificacionPage() {
		baseUrl = "https://en.wikipedia.org";
		driver.get(baseUrl);

		try {
			JavascriptExecutor js = (JavascriptExecutor)driver;
			String last = (String)js.executeScript("return document.lastModified");
			String estado = (String)js.executeScript("return document.readyState");
			LOG.info("ultima modificacion: " + last);
			LOG.info("Estado de la pagina: " + estado);
			
			Reporter.log("Log de reporte");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
