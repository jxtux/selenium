package edu.cibertec.webdriver.ajax;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AjaxTest {

	private static WebDriver driver;
	private WebDriverWait wait;
	String baseUrl;

	@BeforeClass
	public static void openDriver() {
		System.setProperty("webdriver.chrome.driver", "C:\\testing\\programas\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void ancestorTable() {
		String baseUrl = "http://demo.guru99.com/selenium/ajax.html";
		driver.get(baseUrl);

		wait = new WebDriverWait(driver, 5);
		By container = By.cssSelector(".container");
		wait.until(ExpectedConditions.presenceOfElementLocated(container));

		// texto a mostrar luego de la accion ajax por defecto vacio
		WebElement noTextElement = driver.findElement(By.className("radiobutton"));
		String textBefore = noTextElement.getText().trim();

		// Click on the radio button
		driver.findElement(By.id("yes")).click();

		// Click on Check Button
		driver.findElement(By.id("buttoncheck")).click();

		//texto a mostrar luego del la accion ajax
		WebElement TextElement = driver.findElement(By.className("radiobutton"));
		wait.until(ExpectedConditions.visibilityOf(TextElement));
		String textAfter = TextElement.getText().trim();

		/* Verificar texto inicial y despues del ajax */
		Assert.assertNotEquals(textBefore, textAfter);
		System.out.println("Ajax Call Performed");

		String expectedText = "Radio button is checked and it's value is Yes";

		/* Verificar el texto esperado */
		Assert.assertEquals(textAfter, expectedText);
		driver.close();
	}

}
