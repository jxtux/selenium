package edu.cibertec.webdriver.formulario;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class EnlaceTest {

	private static WebDriver driver;
	String baseUrl;

	@BeforeClass
	public static void openDriver() {
		System.setProperty("webdriver.chrome.driver", "C:\\testing\\programas\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	// @Test
	public void enlaceTextoTotal() {
		// LOGIN forma 1
		baseUrl = "http://demo.guru99.com/selenium/link.html";
		driver.get(baseUrl);

		driver.findElement(By.linkText("click here")).click();
		// driver.quit();
	}

	// @Test
	public void enlaceTextoParcial() {
		// LOGIN forma 1
		baseUrl = "http://demo.guru99.com/selenium/link.html";
		driver.get(baseUrl);

		driver.findElement(By.partialLinkText("here")).click();
		// driver.quit();
	}

//	 @Test
	public void enlaceTextoSensitive() {
		baseUrl = "http://demo.guru99.com/selenium/newtours/";
		driver.get(baseUrl);

		String theLinkText = driver.findElement(By.partialLinkText("egis")).getText();
		System.out.println(theLinkText);
		theLinkText = driver.findElement(By.partialLinkText("EGIS")).getText();
		System.out.println(theLinkText);
	}

	 @Test
	public void probarTodosEnlaces() {
		String baseUrl = "http://demo.guru99.com/selenium/newtours/";
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(baseUrl);

		List<WebElement> linkElements = driver.findElements(By.tagName("a"));
		String[] linkTexts = new String[linkElements.size()];
		String underConsTitle = "Under Construction: Mercury Tours";
		int i = 0;

		for (WebElement e : linkElements) {
			linkTexts[i] = e.getText();
			i++;
		}

		for (String t : linkTexts) {
			driver.findElement(By.linkText(t)).click();

			AssertJUnit.assertEquals(driver.getTitle(), underConsTitle);

			if (driver.getTitle().equals(underConsTitle)) {
				System.out.println(t + " is under construction.");
			} else {
				System.out.println(t + " is working.");
			}
			driver.navigate().back();
		}
		driver.quit();
	}

//	@Test
	public void enlaceImagen() {
		String baseUrl = "https://www.facebook.com/login/identify?ctx=recover";
		driver.get(baseUrl);
		driver.findElement(By.cssSelector("a[title=\"Ir a la página de inicio de Facebook\"]")).click();

		if (driver.getTitle().equals("¿Has olvidado la contraseña? | No puedo entrar | Facebook")) {
			System.out.println("Estamos de vuelta en la página de inicio de Facebook");
		} else {
			System.out.println("NO estamos en la página de inicio de Facebook");
		}
		driver.close();
	}

}
