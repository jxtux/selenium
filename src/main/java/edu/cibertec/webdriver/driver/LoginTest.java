package edu.cibertec.webdriver.driver;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginTest {
	private static final Logger LOG = LoggerFactory.getLogger(LoginTest.class);
	private static WebDriver driver;
	private String baseUrl = "https://the-internet.herokuapp.com/login";

	@BeforeClass
	public static void openDriver() {
		// declaración y creación de instancias de objetos y variables
		System.setProperty("webdriver.chrome.driver", "C:\\testing\\programas\\chromedriver.exe");
		driver = new ChromeDriver();
		
		LOG.info("Driver creado!!!");
	}

	@AfterClass
	public static void closeDriver() {
		// cierra el navegador
		//driver.quit();
		
		//termina el programa java pero no cierra el explorador
		System.exit(0);

		// NOTA: driver.close() hace una especie de reset puedes seguir haciendo cosas
		// con el

		LOG.info("Driver terminado!!");
	}

	@Test
	public void loginValido() {
		LOG.info("---------loginValido");
        //iniciar una nueva sesión del navegador y lo dirige a la URL que especifique como su parámetro.
		driver.get(baseUrl);
		
		driver.findElement(By.id("username")).sendKeys("tomsmith");
		driver.findElement(By.id("password")).sendKeys("SuperSecretPassword!");
		driver.findElement(By.className("radius")).click(); // className Css que se usa
		String message = driver.findElement(By.id("flash")).getText();

		boolean contains = message.contains("You logged into a secure area!");
		AssertJUnit.assertTrue("Mensaje de longin Valido es fallido(no es igual al esperado)", contains);
	}

//	@Test
	public void loginUserInvalido() {
		LOG.info("---------login User InValido");
		driver.get(baseUrl);
		driver.findElement(By.id("username")).sendKeys("xxxxx");
		driver.findElement(By.id("password")).sendKeys("SuperSecretPassword!");
		driver.findElement(By.className("radius")).click(); // className Css que se usa
		String message = driver.findElement(By.id("flash")).getText();

		boolean contains = message.contains("Your username is invalid!");
		AssertJUnit.assertTrue("Mensaje de User InValido es fallido(no es igual al esperado)", contains);
	}

//	@Test
	public void loginPasswordInvalido() {
		LOG.info("---------password InValido");
		driver.get(baseUrl);
		driver.findElement(By.id("username")).sendKeys("tomsmith");
		driver.findElement(By.id("password")).sendKeys("xxxxxx");
		driver.findElement(By.className("radius")).click(); // className Css que se usa
		String message = driver.findElement(By.id("flash")).getText();

		boolean contains = message.contains("Your password is invalid!");
		AssertJUnit.assertTrue("Mensaje de Password InValido es fallido(no es igual al esperado)", contains);
	}
}
